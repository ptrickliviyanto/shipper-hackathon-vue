import Vue from 'vue'
import App from './App.vue'

import Amplify from 'aws-amplify'
import '@aws-amplify/ui-vue'
import awsmobile from './aws-exports'
import vuetify from '@/plugins/vuetify' // path to vuetify export
import { AmazonAIPredictionsProvider} from '@aws-amplify/predictions'


Amplify.configure(awsmobile)
Amplify.addPluggable(new AmazonAIPredictionsProvider())

Vue.config.productionTip = false

new Vue({
  vuetify,
  render: h => h(App),
}).$mount('#app')
