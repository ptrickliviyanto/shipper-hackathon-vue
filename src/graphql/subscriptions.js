/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateInbound = /* GraphQL */ `
  subscription OnCreateInbound {
    onCreateInbound {
      id
      po_no
      warehouse_code
      tenant_code
      invoice_no
      shipment_type
      status
      grn_date
      received_qty
      expected_qty
      grn_qty
      good_qty
      damage_qty
      batch
      serial
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateInbound = /* GraphQL */ `
  subscription OnUpdateInbound {
    onUpdateInbound {
      id
      po_no
      warehouse_code
      tenant_code
      invoice_no
      shipment_type
      status
      grn_date
      received_qty
      expected_qty
      grn_qty
      good_qty
      damage_qty
      batch
      serial
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteInbound = /* GraphQL */ `
  subscription OnDeleteInbound {
    onDeleteInbound {
      id
      po_no
      warehouse_code
      tenant_code
      invoice_no
      shipment_type
      status
      grn_date
      received_qty
      expected_qty
      grn_qty
      good_qty
      damage_qty
      batch
      serial
      createdAt
      updatedAt
    }
  }
`;
export const onCreateScanItem = /* GraphQL */ `
  subscription OnCreateScanItem {
    onCreateScanItem {
      id
      brand_id
      status
      barcode
      cart
      model_id
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateScanItem = /* GraphQL */ `
  subscription OnUpdateScanItem {
    onUpdateScanItem {
      id
      brand_id
      status
      barcode
      cart
      model_id
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteScanItem = /* GraphQL */ `
  subscription OnDeleteScanItem {
    onDeleteScanItem {
      id
      brand_id
      status
      barcode
      cart
      model_id
      createdAt
      updatedAt
    }
  }
`;
export const onCreateBrand = /* GraphQL */ `
  subscription OnCreateBrand {
    onCreateBrand {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateBrand = /* GraphQL */ `
  subscription OnUpdateBrand {
    onUpdateBrand {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteBrand = /* GraphQL */ `
  subscription OnDeleteBrand {
    onDeleteBrand {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
