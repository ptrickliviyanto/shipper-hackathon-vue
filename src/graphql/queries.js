/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getInbound = /* GraphQL */ `
  query GetInbound($id: ID!) {
    getInbound(id: $id) {
      id
      po_no
      warehouse_code
      tenant_code
      invoice_no
      shipment_type
      status
      grn_date
      received_qty
      expected_qty
      grn_qty
      good_qty
      damage_qty
      batch
      serial
      createdAt
      updatedAt
    }
  }
`;
export const listInbounds = /* GraphQL */ `
  query ListInbounds(
    $filter: ModelInboundFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listInbounds(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        po_no
        warehouse_code
        tenant_code
        invoice_no
        shipment_type
        status
        grn_date
        received_qty
        expected_qty
        grn_qty
        good_qty
        damage_qty
        batch
        serial
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getScanItem = /* GraphQL */ `
  query GetScanItem($id: ID!) {
    getScanItem(id: $id) {
      id
      brand_id
      status
      barcode
      cart
      model_id
      createdAt
      updatedAt
    }
  }
`;
export const listScanItems = /* GraphQL */ `
  query ListScanItems(
    $filter: ModelScanItemFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listScanItems(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        brand_id
        status
        barcode
        cart
        model_id
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getBrand = /* GraphQL */ `
  query GetBrand($id: ID!) {
    getBrand(id: $id) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const listBrands = /* GraphQL */ `
  query ListBrands(
    $filter: ModelBrandFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listBrands(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
