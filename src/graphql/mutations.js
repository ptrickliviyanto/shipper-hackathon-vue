/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createInbound = /* GraphQL */ `
  mutation CreateInbound(
    $input: CreateInboundInput!
    $condition: ModelInboundConditionInput
  ) {
    createInbound(input: $input, condition: $condition) {
      id
      po_no
      warehouse_code
      tenant_code
      invoice_no
      shipment_type
      status
      grn_date
      received_qty
      expected_qty
      grn_qty
      good_qty
      damage_qty
      batch
      serial
      createdAt
      updatedAt
    }
  }
`;
export const updateInbound = /* GraphQL */ `
  mutation UpdateInbound(
    $input: UpdateInboundInput!
    $condition: ModelInboundConditionInput
  ) {
    updateInbound(input: $input, condition: $condition) {
      id
      po_no
      warehouse_code
      tenant_code
      invoice_no
      shipment_type
      status
      grn_date
      received_qty
      expected_qty
      grn_qty
      good_qty
      damage_qty
      batch
      serial
      createdAt
      updatedAt
    }
  }
`;
export const deleteInbound = /* GraphQL */ `
  mutation DeleteInbound(
    $input: DeleteInboundInput!
    $condition: ModelInboundConditionInput
  ) {
    deleteInbound(input: $input, condition: $condition) {
      id
      po_no
      warehouse_code
      tenant_code
      invoice_no
      shipment_type
      status
      grn_date
      received_qty
      expected_qty
      grn_qty
      good_qty
      damage_qty
      batch
      serial
      createdAt
      updatedAt
    }
  }
`;
export const createScanItem = /* GraphQL */ `
  mutation CreateScanItem(
    $input: CreateScanItemInput!
    $condition: ModelScanItemConditionInput
  ) {
    createScanItem(input: $input, condition: $condition) {
      id
      brand_id
      status
      barcode
      cart
      model_id
      createdAt
      updatedAt
    }
  }
`;
export const updateScanItem = /* GraphQL */ `
  mutation UpdateScanItem(
    $input: UpdateScanItemInput!
    $condition: ModelScanItemConditionInput
  ) {
    updateScanItem(input: $input, condition: $condition) {
      id
      brand_id
      status
      barcode
      cart
      model_id
      createdAt
      updatedAt
    }
  }
`;
export const deleteScanItem = /* GraphQL */ `
  mutation DeleteScanItem(
    $input: DeleteScanItemInput!
    $condition: ModelScanItemConditionInput
  ) {
    deleteScanItem(input: $input, condition: $condition) {
      id
      brand_id
      status
      barcode
      cart
      model_id
      createdAt
      updatedAt
    }
  }
`;
export const createBrand = /* GraphQL */ `
  mutation CreateBrand(
    $input: CreateBrandInput!
    $condition: ModelBrandConditionInput
  ) {
    createBrand(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const updateBrand = /* GraphQL */ `
  mutation UpdateBrand(
    $input: UpdateBrandInput!
    $condition: ModelBrandConditionInput
  ) {
    updateBrand(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
export const deleteBrand = /* GraphQL */ `
  mutation DeleteBrand(
    $input: DeleteBrandInput!
    $condition: ModelBrandConditionInput
  ) {
    deleteBrand(input: $input, condition: $condition) {
      id
      name
      createdAt
      updatedAt
    }
  }
`;
